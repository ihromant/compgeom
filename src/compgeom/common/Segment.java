package compgeom.common;

public class Segment {
	public final Point begin;
	public final Point end;
	
	public Segment(Point begin, Point end) {
		this.begin = begin;
		this.end = end;
	}
	
	public static Segment from(String s) {
		String[] spl = s.split("[\\]\\[;]");
		return new Segment(Point.from(spl[1]), Point.from(spl[2]));
	}
	
	public Point toVector() {
		return end.sub(begin);
	}
	
	public Segment reverse() {
		return new Segment(end, begin);
	}
	
	public Point linearComb(Rational at) {
		return new Point(begin.x.mul((Rational.ONE.sub(at))).add(end.x.mul(at)), begin.y.mul((Rational.ONE.sub(at))).add(end.y.mul(at)));
	}
	
	public boolean isInside(Point p) {
		if (isBegin(p) || isEnd(p)) {
			return true;
		}
		if (!p.sub(begin).vectorProduct(p.sub(end)).equals(Rational.ZERO)) {
			return false;
		} else {
			Rational xDiv = this.end.x.sub(this.begin.x);
			if (!xDiv.zero()) {
				Rational rel = p.x.sub(begin.x).div(xDiv);
				return rel.between(Rational.ZERO, Rational.ONE);
			} else {
				Rational yDiv = this.end.y.sub(this.begin.y);
				Rational rel = p.y.sub(begin.y).div(yDiv);
				return rel.between(Rational.ZERO, Rational.ONE);
			}
		}
	}
	
	public Point intersection(Segment other) {
		Point p = this.begin;
        Point r = this.toVector();
        Point q = other.begin;
        Point s = other.toVector();
        Rational prod = r.vectorProduct(s);
        if (prod.zero()) {
            return null;
        } else {
            Point sub = q.sub(p);
            Rational t = sub.vectorProduct(s).div(prod);
            Rational u = sub.vectorProduct(r).div(prod);
            if (t.between(Rational.ZERO, Rational.ONE) && u.between(Rational.ZERO, Rational.ONE)) {
                return this.linearComb(t);
            } else {
                return null;
            }
        }
	}
	
	public boolean isBegin(Point p) {
		return this.begin.equals(p);
	}
	
	public boolean isEnd(Point p) {
		return this.end.equals(p);
	}
	
	public boolean isHorizontal() {
		return this.begin.y.equals(this.end.y);
	}
	
	public boolean geqY(Point p) {
		if (this.begin.y.equals(this.end.y)) {
			return p.x.compareTo(this.end.x) <= 0;
		} else {
			return p.x.compareTo(linearComb(atY(p.y)).x) <= 0;
		}
	}
	
	public Rational atY(Rational y) {
		return y.sub(this.begin.y).div(this.end.y.sub(this.begin.y));
	}
	
	@Override
	public String toString() {
		return "[" + begin.toString() + ";" + end.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + begin.hashCode();
		result = prime * result + end.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Segment other = (Segment) obj;
		return this.begin.equals(other.begin) && this.end.equals(other.end);
	}
}
