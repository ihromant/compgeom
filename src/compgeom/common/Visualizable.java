package compgeom.common;

import java.awt.Graphics;

public interface Visualizable {
	void visualize(Graphics g);
}
