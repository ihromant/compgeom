package compgeom.common;

public class Point implements Comparable<Point> {
	public final Rational x;
	public final Rational y;

	public Point(long x, long y) {
		this(new Rational(x), new Rational(y));
	}
	
	public Point(Rational x, Rational y) {
		this.x = x;
		this.y = y;
	}
	
	public static Point from(String s) {
		String[] spl = s.split("[,\\)\\(]");
		return new Point(Long.valueOf(spl[1]), Long.valueOf(spl[2]));
	}

	public Point add(Point that) {
		return new Point(this.x.add(that.x), this.y.add(that.y));
	}

	public Point sub(Point that) {
		return new Point(this.x.sub(that.x), this.y.sub(that.y));
	}

	public Rational vectorProduct(Point that) {
		return this.x.mul(that.y).sub(this.y.mul(that.x));
	}

	@Override
	public int compareTo(Point that) {
		int dy = this.y.compareTo(that.y);
		if (dy == 0) {
			return this.x.compareTo(that.x);
		} else {
			return dy;
		}
	}

	@Override
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x.hashCode();
		result = prime * result + y.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		Point other = (Point) o;
		return this.x.equals(other.x) && this.y.equals(other.y);
	}
}
