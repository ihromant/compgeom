package compgeom.common;

import java.math.BigInteger;

public class Rational implements Comparable<Rational> {
	public static final Rational ZERO = new Rational(0);
	public static final Rational ONE = new Rational(1);
	private final BigInteger numerator;
	private final BigInteger denominator;

	public Rational(long numerator) {
		this.numerator = new BigInteger(String.valueOf(numerator));
		this.denominator = BigInteger.ONE;
	}

	public Rational(BigInteger numerator, BigInteger denominator) {
		BigInteger gcd = numerator.abs().gcd(denominator.abs());
		if (denominator.compareTo(BigInteger.ZERO) < 0) {
			this.numerator = numerator.divide(gcd).negate();
			this.denominator = denominator.divide(gcd).negate();
		} else {
			this.numerator = numerator.divide(gcd);
			this.denominator = denominator.divide(gcd);
		}
	}

	public Rational add(Rational that) {
		return new Rational(this.numerator.multiply(that.denominator).add(this.denominator.multiply(that.numerator)), this.denominator.multiply(that.denominator));
	}

	public Rational sub(Rational that) {
		return new Rational(this.numerator.multiply(that.denominator).subtract(this.denominator.multiply(that.numerator)), this.denominator.multiply(that.denominator));
	}

	public Rational mul(Rational that) {
		return new Rational(this.numerator.multiply(that.numerator), this.denominator.multiply(that.denominator));
	}

	public Rational div(Rational that) {
		return new Rational(this.numerator.multiply(that.denominator), this.denominator.multiply(that.numerator));
	}
	
	public boolean between(Rational lower, Rational upper) {
		return this.compareTo(lower) >= 0 && this.compareTo(upper) <= 0;
	}

	public boolean zero() {
		return this.numerator.equals(BigInteger.ZERO);
	}

	@Override
	public int compareTo(Rational that) {
		return this.numerator.multiply(that.denominator).subtract(this.denominator.multiply(that.numerator)).signum();
	}

	public int intValue() {
		return this.numerator.divide(this.denominator).intValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + denominator.intValue();
		result = prime * result + numerator.intValue();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Rational that = (Rational) obj;
		return this.numerator.equals(that.numerator)
				&& this.denominator.equals(that.denominator);
	}

	@Override
	public String toString() {
		if (this.denominator.equals(BigInteger.ONE)) {
			return String.valueOf(this.numerator);
		} else {
			return this.numerator + "/" + this.denominator;
		}
	}
}
