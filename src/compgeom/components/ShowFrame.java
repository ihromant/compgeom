package compgeom.components;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import compgeom.common.Visualizable;


public class ShowFrame extends JFrame {
	private static final long serialVersionUID = 3267854082641693715L;

	public ShowFrame(int size, Visualizable vis) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    final ShowComponent comp = new ShowComponent(vis);
	    comp.setPreferredSize(new Dimension(size, size));
	    getContentPane().add(comp, BorderLayout.CENTER);
	    pack();
	}
}
