package compgeom.components;
import java.awt.Graphics;

import javax.swing.JComponent;

import compgeom.common.Visualizable;

public class ShowComponent extends JComponent {
	private static final long serialVersionUID = -6423076033425896224L;
	private Visualizable vis;

	public ShowComponent(Visualizable vis) {
		this.vis = vis;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		vis.visualize(g);
	}
}
