package compgeom.graham;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import compgeom.common.Point;
import compgeom.common.Rational;
import compgeom.common.Segment;
import compgeom.common.Visualizable;


public class Graham implements Visualizable {
	public List<Point> points;
	
	public Graham(List<Point> points) {
		this.points = points;
	}
	
	public List<Segment> boundary() {
		Collections.sort(points);

		LinkedList<Segment> result = new LinkedList<Segment>();
		result.add(new Segment(points.get(0), points.get(1)));
		for (int i = 2; i < points.size(); i++) {
			result.add(new Segment(result.getLast().end, points.get(i)));
			check(result);
		}
		
		Collections.sort(points, new Comparator<Point>() {
			@Override
			public int compare(Point first, Point second) {
				return -first.compareTo(second);		
			}			
		});
		
		LinkedList<Segment> otherDirection = new LinkedList<Segment>();
		otherDirection.add(new Segment(points.get(0), points.get(1)));
		for (int i = 2; i < points.size(); i++) {
			otherDirection.add(new Segment(otherDirection.getLast().end, points.get(i)));
			check(otherDirection);
		}
		
		for (Segment s : otherDirection) {
			result.add(s);
		}
		
		return result;
	}
	
	private static void check(LinkedList<Segment> segments) {
		if (segments.size() < 2) return;
		Segment last = segments.removeLast();
		Segment preLast = segments.getLast();
		if (last.toVector().vectorProduct(preLast.toVector()).compareTo(Rational.ZERO) >= 0) {
			segments.removeLast();
			segments.add(new Segment(preLast.begin, last.end));
			check(segments);
		} else {
			segments.add(last);
		}
	}

	@Override
	public void visualize(Graphics g) {
		g.setColor(Color.BLACK);
		for (Point p : points) {
			g.drawOval(p.x.intValue(), p.y.intValue(), 1, 1);
		}
		g.setColor(Color.RED);
		List<Segment> segments = boundary();
		for (Segment segment : segments) {
			g.drawLine(segment.begin.x.intValue(), segment.begin.y.intValue(), segment.end.x.intValue(), segment.end.y.intValue());
		}
		g.setColor(Color.BLUE);
		for (Segment segment : segments) {
			Point begin = segment.begin;
			g.drawOval(begin.x.intValue() - 1, begin.y.intValue() - 1, 3, 3);
		}
	}
}
