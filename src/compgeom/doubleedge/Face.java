package compgeom.doubleedge;

import java.util.List;

import compgeom.common.Segment;

public class Face {
	private final List<Segment> boundary;
	
	public Face(List<Segment> boundary) {
		this.boundary = boundary;
	}
	
	public List<Segment> getBoundary() {
		return boundary;
	}
	
	@Override
	public String toString() {
		return "Face: " + boundary;
	}
}
