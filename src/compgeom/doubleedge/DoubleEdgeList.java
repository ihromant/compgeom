package compgeom.doubleedge;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import compgeom.common.Point;
import compgeom.common.Rational;
import compgeom.common.Segment;
import compgeom.sweep.Incident;

public class DoubleEdgeList {
	public final Map<Point, Segment> vertices = new HashMap<Point, Segment>();
	public final Map<Face, FaceInfo> faces = new HashMap<Face, FaceInfo>();
	public final Map<Segment, EdgeInfo> edges = new HashMap<Segment, EdgeInfo>();
	
	private DoubleEdgeList() {
		
	}
	
	public DoubleEdgeList(Polygon poly, Color color) {
		List<Segment> boundary = poly.getSegments();
		List<Segment> reversed = new ArrayList<Segment>();
		int size = boundary.size();
		for (int i = size - 1; i >= 0; i--) {
			reversed.add(boundary.get(i).reverse());
		}
		Face innerFace = new Face(boundary);
		Face outerFace = new Face(reversed);
		faces.put(innerFace, new FaceInfo(boundary.get(0), null, color));
		List<Segment> inner = new ArrayList<Segment>();
		inner.add(reversed.get(0));
		faces.put(outerFace, new FaceInfo(null, inner, null));
		for (Segment s : boundary) {
			vertices.put(s.begin, s);
		}
		for (int i = 0; i < size; i++) {
			edges.put(boundary.get(i), new EdgeInfo(boundary.get(i).begin, reversed.get(size - i - 1), innerFace, getNext(i, boundary), getPrevious(i, boundary)));
			edges.put(reversed.get(i), new EdgeInfo(reversed.get(i).begin, boundary.get(size - i - 1), outerFace, getNext(i, reversed), getPrevious(i, reversed)));
		}
	}
	
	public DoubleEdgeList mapOverlay(DoubleEdgeList other) {
		DoubleEdgeList result = new DoubleEdgeList();
		result.edges.putAll(this.edges);
		result.edges.putAll(other.edges);
		result.vertices.putAll(this.vertices);
		result.vertices.putAll(other.vertices);
		Set<Segment> segments = new HashSet<Segment>();
		for (Segment s : result.edges.keySet()) {
			if (s.begin.compareTo(s.end) < 0) {
				segments.add(s);
			}
		}
		SweepImpr s = new SweepImpr(segments, result);
		Map<Point, Segment> leftmosts = s.processEdges();
		for (Entry<Segment, EdgeInfo> e : result.edges.entrySet()) {
			if (this.faces.containsKey(e.getValue().incidentFace) || other.faces.containsKey(e.getValue().incidentFace)) {
				computeCycle(e.getKey(), result, this, other);
			}
		}
		return result;
	}
	
	private void computeCycle(Segment segm, DoubleEdgeList list, DoubleEdgeList first, DoubleEdgeList second) {
		List<Segment> segments = new ArrayList<Segment>();
		Face face = new Face(segments);
		Segment current = segm;
		Color color = null;
		do {
			segments.add(current);
			color = computeColor(color, first.faces.get(list.edges.get(current).incidentFace) != null ? first.faces.get(list.edges.get(current).incidentFace).color : null, second.faces.get(list.edges.get(current).incidentFace) != null ? second.faces.get(list.edges.get(current).incidentFace).color : null);
			list.edges.get(current).incidentFace = face;
			current = list.edges.get(current).next;
		} while (!current.equals(segm));
		FaceInfo info;
		if (color == null) {
			info = new FaceInfo(null, Arrays.asList(segm), color);
		} else {
			info = new FaceInfo(segm, null, color);
		}
		list.faces.put(face, info);
	}
	
	private Color computeColor(Color... colors) {
		Color current = null;
		for (Color color : colors) {
			if (color != null) {
				if (current == null) {
					current = color;
				} else {
					current = new Color(Math.max(color.getRed(), current.getRed()), Math.max(color.getGreen(), current.getGreen()), Math.max(color.getBlue(), current.getBlue()));
				}
			}
		}
		return current;
	}
	
	private static Segment getPrevious(int i, List<Segment> list) {
		if (i == 0) {
			return list.get(list.size() - 1);
		} else {
			return list.get(i - 1);
		}
	}
	
	private static Segment getNext(int i, List<Segment> list) {
		if (i == list.size() - 1) {
			return list.get(0);
		} else {
			return list.get(i + 1);
		}
	}
	
	public static class FaceInfo {
		private Segment outerComponent;
		private List<Segment> innerComponents;
		public Color color;
		
		private FaceInfo(Segment outerComponent, List<Segment> innerComponents, Color color) {
			this.outerComponent = outerComponent;
			this.innerComponents = innerComponents;
			this.color = color;
		}
	}
	
	private static class EdgeInfo {
		private Point origin;
		private Segment twin;
		private Face incidentFace;
		private Segment next;
		private Segment previous;
		
		private EdgeInfo(Point origin, Segment twin, Face incidentFace,
				Segment next, Segment previous) {
			this.origin = origin;
			this.twin = twin;
			this.incidentFace = incidentFace;
			this.next = next;
			this.previous = previous;
		}
		
		private EdgeInfo(EdgeInfo that) {
			this.origin = that.origin;
			this.twin = that.twin;
			this.incidentFace = that.incidentFace;
			this.next = that.next;
			this.previous = that.previous;
		}
		
		@Override
		public String toString() {
			return String.format("prev: %s, next: %s", previous, next);
		}
	}

	private static class SweepImpr {
		private TreeMap<Point, Incident> incidents = new TreeMap<Point, Incident>();
		private List<Segment> status = new LinkedList<Segment>();
		private Map<Segment, List<Point>> leftmosts = new HashMap<Segment, List<Point>>();
		private Collection<Segment> segments;
		private DoubleEdgeList toFix;
		public SweepImpr(Collection<Segment> segments, DoubleEdgeList toFix) {
			this.segments = segments;
			this.toFix = toFix;
		}
		
		private void addByPoint(Point point, Segment segment) {
			Incident current = incidents.get(point);
			if (current != null) {
				current.add(segment);
			} else {
				Incident inc = new Incident(point);
				inc.add(segment);
				incidents.put(point, inc);
			}
		}
		
		public Map<Point, Segment> processEdges() {
			for (Segment segment : segments) {
				addByPoint(segment.begin, segment);
				addByPoint(segment.end, segment);
			}
			while (!incidents.isEmpty()) {
				Incident incident = incidents.remove(incidents.firstKey());
				Segment left = handleIncident(incident);
				if (left != null) {
					if (!leftmosts.containsKey(left)) {
						List<Point> points = new ArrayList<Point>();
						points.add(incident.point);
						leftmosts.put(left, points);
					} else {
						leftmosts.get(left).add(incident.point);
					}
				}
			}
			Map<Point, Segment> result = new HashMap<Point, Segment>();
			for (Entry<Segment, List<Point>> e : leftmosts.entrySet()) {
				for (Point p : e.getValue()) {
					result.put(p, e.getKey());
				}
			}
			return result;
		}
		
		private Segment handleIncident(Incident incident) {
			Segment result = null;
			ListIterator<Segment> iter = status.listIterator();
			while (iter.hasNext()) {
				Segment current = iter.next();
				if (current.geqY(incident.point)) {
					iter.previous();
					break;
				}
			}
			while (iter.hasNext()) {
				Segment current = iter.next();
				if (!current.isInside(incident.point)) {
					iter.previous();
					break;
				} else {
					incident.add(current);
					iter.remove();
				}
			}
			changeIncident(incident);
			if (revalidateSets(incident.getLp(), incident.getUp())) {
				int counter = 0;
				while (iter.hasPrevious()) {
					Segment curr = iter.previous();
					counter++;
					if (!curr.isHorizontal()) {
						result = curr.reverse();
						break;
					}
				}
				for (int i = 0; i < counter; i++) {
					iter.next();
				}
			}
			handleBeforeAdding(iter, incident);
			Iterator<Segment> toAdd = incident.getOrdered();
			if (toAdd.hasNext()) {
				boolean first = true;
				Segment current;
				do {
					current = toAdd.next();
					if (first) {
						first = false;
						if (iter.hasPrevious()) {
							Segment prev = iter.previous();
							iter.next();
							findNewEvent(prev, current, incident.point);
						}
					}
					iter.add(current);
				} while (toAdd.hasNext());
				if (iter.hasNext()) {
					findNewEvent(current, iter.next(), incident.point);
				}
			}
			return result;
		}
		
		private void handleBeforeAdding(ListIterator<Segment> statusIter, Incident incident) {
			if (statusIter.hasNext() && statusIter.hasPrevious()) {
				Segment prev = statusIter.previous();
				statusIter.next();
				Segment next = statusIter.next();
				statusIter.previous();
				if (incident.getCp().isEmpty() && incident.getUp().isEmpty()) {
					findNewEvent(prev, next, incident.point);
				} else {
					Point p = prev.intersection(next);
					if (p != null && incidents.containsKey(p)) {
						Incident inc = incidents.get(p);
						inc.remove(prev);
						inc.remove(next);
						if (inc.size() == 0) {
							incidents.remove(p);
						}
					}
				}
			}
		}
		
		private void changeIncident(Incident inc) {
			NavigableSet<Segment> cp = inc.getCp();
			Iterator<Segment> iter = cp.iterator();
			NavigableSet<Segment> copy = new TreeSet<Segment>(cp);
			while (iter.hasNext()) {
				Segment next = iter.next();
				
				Segment s = new Segment(next.begin, inc.point);
				inc.add(s);
				addEdgeInfoIfNotPresent(s, next);
				if (leftmosts.containsKey(next.reverse())) {
					List<Point> points = leftmosts.remove(next.reverse());
					leftmosts.put(s.reverse(), points);
				}
				
				s = new Segment(inc.point, next.end);
				inc.add(s);
				addEdgeInfoIfNotPresent(s, next);
				if (incidents.containsKey(next.end)) {
					Incident endInc = incidents.get(next.end);
					endInc.remove(next);
					endInc.add(s);
				}
				iter.remove();
			}
			for (Segment s : copy) {
				toFix.edges.get(toFix.edges.get(s).previous).next = new Segment(s.begin, inc.point);
				toFix.edges.get(toFix.edges.get(s).next).previous = new Segment(inc.point, s.end);
				toFix.edges.remove(s);
				Segment reverse = s.reverse();
				toFix.edges.get(toFix.edges.get(reverse).next).previous = new Segment(inc.point, reverse.end);
				toFix.edges.get(toFix.edges.get(reverse).previous).next = new Segment(reverse.begin, inc.point);
				toFix.edges.remove(reverse);
			}
		}
		
		private boolean revalidateSets(NavigableSet<Segment> lp, NavigableSet<Segment> up) {
			boolean result = false;
			Segment prev;
			if (!lp.isEmpty()) {
				Iterator<Segment> lpIter = lp.iterator();
				prev = lpIter.next();
				result = improveEdgeInfo(toFix.edges.get(!up.isEmpty() ? up.last() : lp.last().reverse()).twin, toFix.edges.get(prev).twin) || result;
				while (lpIter.hasNext()) {
					Segment current = lpIter.next();
					result = improveEdgeInfo(toFix.edges.get(prev.reverse()).twin, toFix.edges.get(current).twin) || result;
					prev = current;
				}
			}
			if (!up.isEmpty()) {
				Iterator<Segment> upIter = up.iterator();
				prev = upIter.next();
				result = improveEdgeInfo(toFix.edges.get(!lp.isEmpty() ? lp.last().reverse() : up.last()).twin, toFix.edges.get(prev.reverse()).twin) || result;
				while (upIter.hasNext()) {
					Segment current = upIter.next();
					result = improveEdgeInfo(toFix.edges.get(prev).twin, toFix.edges.get(current.reverse()).twin) || result;
					prev = current;
				}
			}
			return result;
		}
		
		private boolean improveEdgeInfo(Segment first, Segment second) {
			toFix.edges.get(first).next = second;
			toFix.edges.get(second).previous = first;
			return first.toVector().x.compareTo(Rational.ZERO) <= 0 && second.toVector().x.compareTo(Rational.ZERO) >= 0;
		}
		
		private void addEdgeInfoIfNotPresent(Segment s, Segment basic) {
			if (!toFix.edges.containsKey(s)) {
				Segment reverse = s.reverse();
				EdgeInfo info = new EdgeInfo(toFix.edges.get(basic));
				info.twin = reverse;
				toFix.edges.put(s, info);
				info = new EdgeInfo(toFix.edges.get(basic.reverse()));
				info.twin = s;
				toFix.edges.put(reverse, info);
			}
		}
		
		private void findNewEvent(Segment first, Segment second, Point p) {
			Point intersection = first.intersection(second);
			if (intersection != null && (intersection.y.compareTo(p.y) > 0 || intersection.y.compareTo(p.y) == 0 && intersection.x.compareTo(p.x) > 0)) {
				if (incidents.containsKey(intersection)) {
					Incident incident = incidents.get(intersection);
					incident.add(first);
					incident.add(second);
				} else {
					Incident newIncident = new Incident(intersection);
					newIncident.add(first);
					newIncident.add(second);
					incidents.put(intersection, newIncident);
				}
			}
		}
	}
}
