package compgeom.doubleedge;

import java.util.ArrayList;
import java.util.List;

import compgeom.common.Point;
import compgeom.common.Segment;

public class Polygon {
	private final List<Segment> segments;

	public Polygon(List<Segment> segments) {
		if (segments.get(0).begin.equals(segments.get(segments.size() - 1).end)) {
			this.segments = segments;
		} else {
			Segment last = new Segment(segments.get(segments.size() - 1).end, segments.get(0).begin);
			this.segments = new ArrayList<Segment>(segments);
			this.segments.add(last);
		}
	}

	public Polygon(Point... points) {
		this.segments = new ArrayList<Segment>();
		for (int i = 0; i < points.length - 1; i++) {
			segments.add(new Segment(points[i], points[i + 1]));
		}
		segments.add(new Segment(points[points.length - 1], points[0]));
	}

	public List<Segment> getSegments() {
		return this.segments;
	}
}
