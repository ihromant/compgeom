package compgeom.doubleedge;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import compgeom.common.Point;
import compgeom.common.Segment;
import compgeom.common.Visualizable;
import compgeom.doubleedge.DoubleEdgeList.FaceInfo;

public class BoolOps implements Visualizable {
	private Polygon first;
	private Polygon second;
	int stage = 0;

	public BoolOps(Polygon first, Polygon second) {
		this.first = first;
		this.second = second;
		//this.first = new Polygon(new Point(700, 350), new Point(350, 700), new Point(0, 350), new Point(350, 0));
		//this.second = new Polygon(new Point(100, 100), new Point(600, 100), new Point(600, 600), new Point(100, 600));
	}

	@Override
	public void visualize(Graphics g) {
		DoubleEdgeList firstList = new DoubleEdgeList(first, new Color(255, 0, 0));
		DoubleEdgeList secondList = new DoubleEdgeList(second, new Color(0, 0, 255));
		switch (stage) {
		case 0:
			draw(g, firstList.faces);
			stage++;
			return;
		case 1:
			g.clearRect(0, 0, 700, 700);
			draw(g, secondList.faces);
			stage++;
			return;
		case 2:
			g.clearRect(0, 0, 700, 700);
			DoubleEdgeList result = firstList.mapOverlay(secondList);
			draw(g, result.faces);
			return;
		}
	}

	private void draw(Graphics g, Map<Face, FaceInfo> info) {
		for (Entry<Face, FaceInfo> e : info.entrySet()) {
			Color color = e.getValue().color;
			if (color != null) {
				List<Segment> boundary = e.getKey().getBoundary();
				int[][] values = new int[2][boundary.size()];
				for (int i = 0; i < boundary.size(); i++) {
					values[0][i] = boundary.get(i).begin.x.intValue();
					values[1][i] = boundary.get(i).begin.y.intValue();
				}
				g.setColor(color);
				g.fillPolygon(values[0], values[1], boundary.size());
				g.setColor(Color.BLACK);
				g.drawPolygon(values[0], values[1], boundary.size());
			}
		}
	}
}
