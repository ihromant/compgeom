package compgeom.sweep;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.TreeSet;

import compgeom.common.Point;
import compgeom.common.Rational;
import compgeom.common.Segment;

public class Incident implements Comparator<Segment> {
	private static final NavigableSet<Segment> EMPTY = new TreeSet<Segment>();
	public final Point point;
	
	private NavigableSet<Segment> lp;
	private NavigableSet<Segment> cp;
	private NavigableSet<Segment> up;
	
	public Incident(Point point) {
		this.point = point;
	}
	
	public void add(Segment segment) {
		if (segment.isBegin(point)) {
			if (up == null) {
				up = new TreeSet<Segment>(this);
			}
			up.add(segment);
		} else if (segment.isEnd(point)) {
			if (lp == null) {
				lp = new TreeSet<Segment>(this);
			}
			lp.add(segment);
		} else {
			if (cp == null) {
				cp = new TreeSet<Segment>(this);
			}
			cp.add(segment);
		}
	}
	
	public void remove(Segment segment) {
		if (segment.isBegin(point)) {
			if (up != null) {
				up.remove(segment);
			}
		} else if (segment.isEnd(point)) {
			if (lp != null) {
				lp.remove(segment);
			}
		} else {
			if (cp != null) {
				cp.remove(segment);
			}
		}
	}

	public NavigableSet<Segment> getLp() {
		if (lp == null) {
			return EMPTY;
		} else {
			return lp;
		}
	}

	public NavigableSet<Segment> getCp() {
		if (cp == null) {
			return EMPTY;
		} else {
			return cp;
		}
	}

	public NavigableSet<Segment> getUp() {
		if (up == null) {
			return EMPTY;
		} else {
			return up;
		}
	}
	
	public Segment getLowest() {
		if (!getCp().isEmpty()) {
			if (!getUp().isEmpty()) {
				Segment c = getCp().first();
				Segment u = getUp().first();
				return compare(c, u) < 0 ? c : u;
			} else {
				return getCp().first();
			}
		} else {
			if (!getUp().isEmpty()) {
				return getUp().first();
			} else {
				return null;
			}
		}
	}
	
	public Segment getLargest() {
		if (!getCp().isEmpty()) {
			if (!getUp().isEmpty()) {
				Segment c = getCp().last();
				Segment u = getUp().last();
				return compare(c, u) > 0 ? c : u;
			} else {
				return getCp().last();
			}
		} else {
			if (!getUp().isEmpty()) {
				return getUp().last();
			} else {
				return null;
			}
		}
	}
	
	private int compareVectors(Point first, Point second) {
		if (first.y.equals(Rational.ZERO)) {
			if (second.y.equals(Rational.ZERO)) {
				return 0;
			} else {
				return 1;
			}
		} else {
			if (second.y.equals(Rational.ZERO)) {
				return -1;
			} else {
				return first.x.div(first.y).compareTo(second.x.div(second.y));
			}
		}
	}
	
	@Override
	public int compare(Segment first, Segment second) {
		int compareEndVectors = compareVectors(first.end.sub(point), second.end.sub(point));
		if (compareEndVectors == 0) {
			return compareVectors(first.begin.sub(point), second.begin.sub(point));
		} else {
			return compareEndVectors;
		}
	}
	
	public Iterator<Segment> getOrdered() {
		final Iterator<Segment> cpIter = cp == null ? Collections.<Segment>emptyIterator() : cp.iterator();
		final Iterator<Segment> upIter = up == null ? Collections.<Segment>emptyIterator() : up.iterator();
		return new Iterator<Segment>() {
			Segment cCur = nextCrossing();
			Segment uCur = nextUpper();
			
			private Segment nextCrossing() {
				return cpIter.hasNext() ? cpIter.next() : null;
			}
			
			private Segment nextUpper() {
				return upIter.hasNext() ? upIter.next() : null;
			}

			@Override
			public boolean hasNext() {
				return cCur != null || uCur != null;
			}

			@Override
			public Segment next() {
				Segment result;
				if (cCur != null) {
					if (uCur != null) {
						if (compare(cCur, uCur) < 0) {
							result = cCur;
							cCur = nextCrossing();
						} else {
							result = uCur;
							uCur = nextUpper();
						}
					} else {
						result = cCur;
						cCur = nextCrossing();
					}
				} else {
					if (uCur != null) {
						result = uCur;
						uCur = nextUpper();
					} else {
						throw new NoSuchElementException();
					}
				}
				return result;
			}

			@Override
			public void remove() {
				throw new IllegalStateException();
			}
			
		};
	}
	
	public int size() {
		int result = 0;
		result = result + (lp == null ? 0 : lp.size());
		result = result + (cp == null ? 0 : cp.size());
		result = result + (up == null ? 0 : up.size());
		return result;
	}
	
	@Override
	public String toString() {
		return "[Point: " + point + ", lp: " + lp + ", cp: " + cp + ", up: " + up + "]";
	}
}
