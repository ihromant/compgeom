package compgeom.sweep;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import compgeom.common.Point;
import compgeom.common.Segment;
import compgeom.common.Visualizable;

public class Intersections implements Visualizable {
	protected List<Segment> segments;
	
	public Intersections(List<Segment> segments) {
		this.segments = segments;
	}

	public Set<Point> intersections() {
		Set<Point> result = new HashSet<Point>();
		for (int i = 0; i < segments.size(); i++) {
			for (int j = i + 1; j < segments.size(); j++) {
				Point intersection = segments.get(i).intersection(segments.get(j));
				if (intersection != null) {
					result.add(intersection);
				}
			}
		}
		return result;
	}

	@Override
	public void visualize(Graphics g) {
		g.setColor(Color.BLACK);
		for (Segment segment : segments) {
			g.drawLine(segment.begin.x.intValue(), segment.begin.y.intValue(),
					segment.end.x.intValue(), segment.end.y.intValue());
		}
		g.setColor(Color.RED);
		for (Point p : intersections()) {
			g.drawOval(p.x.intValue() - 1, p.y.intValue() - 1, 3, 3);
		}
	}
}
