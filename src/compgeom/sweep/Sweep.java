package compgeom.sweep;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeMap;

import compgeom.common.Point;
import compgeom.common.Segment;

public class Sweep extends Intersections {
	private TreeMap<Point, Incident> incidents = new TreeMap<Point, Incident>();
	private Set<Point> intersections = new HashSet<Point>();
	private List<Segment> status = new LinkedList<Segment>();
	public Sweep(List<Segment> segments) {
		super(segments);
	}
	
	private void addByPoint(Point point, Segment segment) {
		Incident current = incidents.get(point);
		if (current != null) {
			current.add(segment);
		} else {
			Incident inc = new Incident(point);
			inc.add(segment);
			incidents.put(point, inc);
		}
	}
	
	@Override
	public Set<Point> intersections() {
		for (Segment segment : segments) {
			addByPoint(segment.begin, segment);
			addByPoint(segment.end, segment);
		}
		while (!incidents.isEmpty()) {
			Incident incident = incidents.remove(incidents.firstKey());
			handleIncident(incident);
		}
		return intersections;
	}
	
	private void handleIncident(Incident incident) {
		ListIterator<Segment> iter = status.listIterator();
		while (iter.hasNext()) {
			Segment current = iter.next();
			if (current.geqY(incident.point)) {
				iter.previous();
				break;
			}
		}
		while (iter.hasNext()) {
			Segment current = iter.next();
			if (!current.isInside(incident.point)) {
				iter.previous();
				break;
			} else {
				incident.add(current);
				iter.remove();
			}
		}
		if (incident.size() > 1) {
			intersections.add(incident.point);
		}
		handleBeforeAdding(iter, incident);
		Iterator<Segment> toAdd = incident.getOrdered();
		if (toAdd.hasNext()) {
			boolean first = true;
			Segment current;
			do {
				current = toAdd.next();
				if (first) {
					first = false;
					if (iter.hasPrevious()) {
						Segment prev = iter.previous();
						iter.next();
						findNewEvent(prev, current, incident.point);
					}
				}
				iter.add(current);
			} while (toAdd.hasNext());
			if (iter.hasNext()) {
				findNewEvent(current, iter.next(), incident.point);
			}
		}
	}
	
	private void handleBeforeAdding(ListIterator<Segment> statusIter, Incident incident) {
		if (statusIter.hasNext() && statusIter.hasPrevious()) {
			Segment prev = statusIter.previous();
			statusIter.next();
			Segment next = statusIter.next();
			statusIter.previous();
			if (incident.getCp().isEmpty() && incident.getUp().isEmpty()) {
				findNewEvent(prev, next, incident.point);
			} else {
				Point p = prev.intersection(next);
				if (p != null && incidents.containsKey(p)) {
					Incident inc = incidents.get(p);
					inc.remove(prev);
					inc.remove(next);
					if (inc.size() == 0) {
						incidents.remove(p);
					}
				}
			}
		}
	}
	
	private void findNewEvent(Segment first, Segment second, Point p) {
		Point intersection = first.intersection(second);
		if (intersection != null && (intersection.y.compareTo(p.y) > 0 || intersection.y.compareTo(p.y) == 0 && intersection.x.compareTo(p.x) > 0)) {
			if (incidents.containsKey(intersection)) {
				Incident incident = incidents.get(intersection);
				incident.add(first);
				incident.add(second);
			} else {
				Incident newIncident = new Incident(intersection);
				newIncident.add(first);
				newIncident.add(second);
				incidents.put(intersection, newIncident);
			}
		}
	}
}