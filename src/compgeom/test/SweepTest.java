package compgeom.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import compgeom.common.Point;
import compgeom.common.Segment;
import compgeom.generators.Generator;
import compgeom.generators.TestSegmentGenerator;
import compgeom.sweep.Intersections;
import compgeom.sweep.Sweep;


public class SweepTest {
	@Test
	public void test() {
		Generator<Segment> gen = new TestSegmentGenerator(50, 600);
		int counter = 0;
		for (int i = 0; i < 10000; i++) {
			List<Segment> toCheck = gen.generate(20);
			Intersections sweep = new Sweep(toCheck);
			Intersections inters = new Intersections(toCheck);
			Set<Point> fromSweep = sweep.intersections();
			Set<Point> fromDirect = inters.intersections();
			if (!fromSweep.equals(fromDirect)) {
				Set<Point> copy = new HashSet<Point>(fromDirect);
				copy.removeAll(fromSweep);
				fromSweep.removeAll(fromDirect);
				fail(fromSweep + "\n" + copy + "\n" + "Failed" + ++counter + " " + toCheck.toString() + "\n");
			}
		}
	}
}
