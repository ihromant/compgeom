package compgeom.generators;

import compgeom.common.Point;
import compgeom.common.Segment;

public class SegmentGenerator extends UniformGenerator<Segment> {
	protected UniformGenerator<Point> pointGenerator;
	
	public SegmentGenerator(int from, int side) {
		this.pointGenerator = new SquarePointGenerator(from, side);
	}
	
	@Override
	public Segment getExample() {
		Point first = pointGenerator.getExample();
		Point second = pointGenerator.getExample();
		while (second.compareTo(first) == 0) {
			second = pointGenerator.getExample();
		}
		if (first.compareTo(second) > 0) {
			return new Segment(second, first);
		} else {
			return new Segment(first, second);
		}
	}
}
