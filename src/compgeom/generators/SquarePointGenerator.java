package compgeom.generators;

import compgeom.common.Point;

public class SquarePointGenerator extends UniformGenerator<Point> {
	private int from;
	private int side;

	public SquarePointGenerator(int from, int side) {
		this.from = from;
		this.side = side;
	}

	@Override
	public Point getExample() {
		return new Point((int) (rand.nextDouble() * side + from),
				(int) (rand.nextDouble() * side + from));
	}
}
