package compgeom.generators;

import java.util.ArrayList;
import java.util.List;

import compgeom.common.Point;
import compgeom.common.Segment;
import compgeom.doubleedge.Polygon;

public class StarPolygonGenerator extends UniformGenerator<Polygon> {
	private static final double ANGLE_STEP = Math.PI * 10 / 180;
	private final Point center;
	private final int innerRadius;
	private final int outerRadius;

	public StarPolygonGenerator(Point center, int innerRadius, int outerRadius) {
		this.center = center;
		this.innerRadius = innerRadius;
		this.outerRadius = outerRadius;
	}

	@Override
	public Polygon getExample() {
		List<Segment> segments = new ArrayList<Segment>();
		Point current = center.add(fromAngleRadius(getRadius(), 0));
		double angle = rand.nextDouble() * ANGLE_STEP;
		while (angle < 2 * Math.PI) {
			Point next = center.add(fromAngleRadius(getRadius(), angle));
			segments.add(new Segment(current, next));
			current = next;
			angle = angle + rand.nextDouble() * ANGLE_STEP;
		}
		return new Polygon(segments);
	}
	
	private int getRadius() {
		return innerRadius + (int) ((outerRadius - innerRadius) * rand.nextDouble());
	}
	
	private Point fromAngleRadius(int radius, double angle) {
		return new Point((int) (radius * Math.cos(angle)), (int) (radius * Math.sin(angle)));
	}
}
