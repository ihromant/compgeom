package compgeom.generators;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class UniformGenerator<T> implements Generator<T> {
	protected Random rand = new Random();
	
	public List<T> generate(int count) {
		List<T> result = new ArrayList<T>(count);
		for (int i=0; i < count; i++) {
			result.add(getExample());
		}
		return result;
	}
	
	public abstract T getExample();
}