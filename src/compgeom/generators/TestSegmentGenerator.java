package compgeom.generators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import compgeom.common.Segment;

public class TestSegmentGenerator extends SegmentGenerator {
	private static final Comparator<Segment> COMP = new Comparator<Segment>() {
		@Override
		public int compare(Segment first, Segment second) {
			int dby = first.begin.y.compareTo(second.begin.y);
			if (dby == 0) {
				int dey = first.end.y.compareTo(second.end.y);
				if (dey == 0) {
					if (first.isHorizontal() && second.isHorizontal()) {
						if (first.end.x.between(second.begin.x, second.end.x) || first.begin.x.between(second.begin.x, second.end.x)) {
							return 0;
						} else {
							return first.end.x.compareTo(second.begin.x);
						}
					} else {
						int dbx = first.begin.x.compareTo(second.begin.x);
						if (dbx == 0) {
							return first.end.x.compareTo(second.end.x);
						} else {
							return dbx;
						}
					}
				} else {
					return dey;
				}
			} else {
				return dby;
			}
		}
	};
	
	public TestSegmentGenerator(int from, int side) {
		super(from, side);
	}

	@Override
	public List<Segment> generate(int count) {
		Set<Segment> presentSegments = new TreeSet<Segment>(COMP);
		while (presentSegments.size() < count) {
			presentSegments.add(getExample());
		}
		return new ArrayList<Segment>(presentSegments);
	}

}
