package compgeom.generators;

import compgeom.common.Point;

public class CirclePointGenerator extends UniformGenerator<Point> {
	private Point center;
	private int maxRadius;

	public CirclePointGenerator(Point center, int maxRadius) {
		this.center = center;
		this.maxRadius = maxRadius;
	}

	@Override
	public Point getExample() {
		double angle = rand.nextDouble() * 2 * Math.PI;
		double radius = rand.nextDouble() * maxRadius;
		return new Point(
				(int) (radius * Math.cos(angle) + center.x.intValue()),
				(int) (radius * Math.sin(angle) + center.y.intValue()));
	}
}
