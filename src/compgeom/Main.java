package compgeom;

import compgeom.common.Point;
import compgeom.components.ShowFrame;
import compgeom.doubleedge.BoolOps;
import compgeom.doubleedge.Polygon;
import compgeom.generators.StarPolygonGenerator;
import compgeom.generators.UniformGenerator;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		UniformGenerator<Polygon> gen = new StarPolygonGenerator(new Point(350, 350), 200, 300);
		//List<Segment> toCheck = gen.generate(20);
		//Intersections inters = new Sweep(toCheck);
		ShowFrame frame = new ShowFrame(700, new BoolOps(gen.getExample(), gen.getExample()));
		frame.setVisible(true);
		Thread.sleep(5000);
		frame.repaint();
		Thread.sleep(5000);
		frame.repaint();
	}
}
